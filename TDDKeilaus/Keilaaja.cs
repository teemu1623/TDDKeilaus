﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TDDKeilaus
{
    public class Keilaaja
    {     
        Random random = new Random();
        public string Nimi { get; set; }
        public int Pisteet { get; set; }
        public int[] heitonPisteet = new int [25];
        public int[] vuoronPisteet = new int[25];
        public int[] yhteisPisteet = new int[25];
        public int[] kaato = new int[25];
        public int[] paikko = new int[25];
        public int heitonNro = 0;

        public int heitäKeilapalloa(int keilat)
        {
            int kaadetutKeilat = random.Next(keilat + 1);
            return kaadetutKeilat;
        }
        public void heitäYksiVuoro(int heitonNumero)
        {
            heitonPisteet[heitonNumero - 1] = heitäKeilapalloa(10);
            if (heitonPisteet[heitonNumero - 1] != 10)
            {
                heitonPisteet[heitonNumero] = heitäKeilapalloa(10 - heitonPisteet[heitonNumero - 1]);
                if (heitonPisteet[heitonNumero - 1] + heitonPisteet[heitonNumero] == 10)
                    paikko[heitonNumero] = 1;
            }
            else
            {
                kaato[heitonNumero - 1] = 1;
            }
            heitonNro++;
        }
        public void heitäViimeinenVuoro()
        {
            heitonNro++;
            heitonPisteet[18] = heitäKeilapalloa(10);

            //Ensimmäinen heitto on kaato
            if (heitonPisteet[18] == 10)
            {
                kaato[18] = 1;
                heitonPisteet[19] = heitäKeilapalloa(10);

                //Toinen heitto on kaato
                if (heitonPisteet[19] == 10)
                {
                    kaato[19] = 1;
                    heitonPisteet[20] = heitäKeilapalloa(10);

                    //Kolmas heitto on kaato
                    if (heitonPisteet[20] == 10)
                    {
                        kaato[20] = 1;
                    }
                }
                //Toinen heitto ei ole kaato
                else
                {
                    heitonPisteet[20] = heitäKeilapalloa(10);

                    //Kolmas heitto on kaato
                    if (heitonPisteet[20] == 10)
                    {
                        kaato[20] = 1;
                    }
                }
            }
            //Ensimmäinen heitto ei ole kaato
            else
            {
                heitonPisteet[19] = heitäKeilapalloa(10 - heitonPisteet[18]);

                //Toinen heitto on paikko
                if (heitonPisteet[18] + heitonPisteet[19] == 10)
                {
                    paikko[19] = 1;
                    heitonPisteet[20] = heitäKeilapalloa(10);

                    //Kolmas heitto on kaato
                    if (heitonPisteet[20] == 10)
                    {
                        kaato[20] = 1;
                    }
                }
            }
        }

        public void LaskePisteet()
        {
            int x = 0;
            for (int i = 0; i < 10; i++)
            {
                vuoronPisteet[i] = 0;
                yhteisPisteet[i] = 0;
            }
            for (int i = 0; i < 9; i++)
            {
                vuoronPisteet[i] = heitonPisteet[x] + heitonPisteet[x + 1];
                if(kaato[x] == 1)
                {
                    if (kaato[x + 2] == 1)
                        vuoronPisteet[i] = vuoronPisteet[i] + heitonPisteet[x + 2] + heitonPisteet[x + 4];
                    else
                        vuoronPisteet[i] = vuoronPisteet[i] + heitonPisteet[x + 2] + heitonPisteet[x + 3];
                }
                if (paikko[x + 1] == 1)
                {
                    vuoronPisteet[i] = vuoronPisteet[i] + heitonPisteet[x + 2];
                }
                x = x + 2;
            }
            //Viimeinen kierros
            vuoronPisteet[9] = heitonPisteet[18] + heitonPisteet[19] + heitonPisteet[20];
            if(kaato[18] == 1)
                vuoronPisteet[9] = vuoronPisteet[9] + heitonPisteet[19] + heitonPisteet[20];
            if(kaato[19] == 1 || paikko[19] == 1)
                vuoronPisteet[9] = vuoronPisteet[9] + heitonPisteet[20];

            yhteisPisteet[0] = vuoronPisteet[0];
            for (int i = 1; i < 10; i++)
            {
                yhteisPisteet[i] = yhteisPisteet[i - 1] + vuoronPisteet[i];
            }
        }

        public void TulostaPisteet()
        {
            LaskePisteet();
            //1. rivi
            //Nimipalkki
            Console.Write("Nimi:       |");
            //Tulospalkki
            for (int i = 0; i < 18; i = i + 2)
            {
                if (heitonPisteet[i] == 10)
                    Console.Write("X  |");
                else if (heitonPisteet[i] + heitonPisteet[i + 1] == 10)
                    Console.Write(heitonPisteet[i] + " /|");
                else
                    Console.Write(heitonPisteet[i] + " " + heitonPisteet[i + 1] + "|");
            }
            //Viimeinen Kierros
            //1. Kaato
            if (heitonPisteet[18] == 10)
            {
                Console.Write("X ");
                //2. Kaato
                if (heitonPisteet[19] == 10)               
                    Console.Write("X ");
                //2. Ei kaato
                else
                    Console.Write(heitonPisteet[19] +" ");
            }
            //1. Ei kaato
            else
            {
                Console.Write(heitonPisteet[18] + " ");

                //2. Paikko
                if (heitonPisteet[18] + heitonPisteet[19] == 10)
                    Console.Write("/ ");
                //. Ei Paikko
                else
                    Console.Write(heitonPisteet[19]+ " ");
            }
            //3. Kaato
            if (heitonPisteet[20] == 10)
                Console.Write("X");
            //3. Ei kaato.
            else
                Console.Write(heitonPisteet[20]);
            Console.WriteLine("|  Pisteet:");

            //2. rivi
            //Nimipalkki
            if (Nimi.Length > 10)
                Console.Write("  " + Nimi.Substring(0, 10));
            else
            {
                Console.Write("  " + Nimi);
                int pituus = 10 - Nimi.Length;
                while (pituus != 0)
                {
                    Console.Write(" ");
                    pituus--;
                }
            }
            Console.Write("|");
            //Tulospalkki            
            for (int i = 0; i < 9; i++)
            {
                if (i < heitonNro)
                {
                    if (yhteisPisteet[i] < 10)
                        Console.Write("  " + yhteisPisteet[i] + "|");
                    else if (yhteisPisteet[i] < 100)
                        Console.Write(" " + yhteisPisteet[i] + "|");
                    else if (yhteisPisteet[i] < 1000)
                        Console.Write("" + yhteisPisteet[i] + "|");
                }
                else
                    Console.Write("   |");
            }
            //Viimeinen kierros
            if (heitonNro > 9)
            {
                if (yhteisPisteet[9] < 10)
                    Console.Write("    " + yhteisPisteet[9] + "|");
                else if (yhteisPisteet[9] < 100)
                    Console.Write("   " + yhteisPisteet[9] + "|");
                else if (yhteisPisteet[9] < 1000)
                    Console.Write("  " + yhteisPisteet[9] + "|");
            }
            else             
                Console.Write("     |");
            Console.WriteLine("     " + yhteisPisteet[9]);        
        }
        public void nollaus()
        {
            for (int i = 0; i < 24; i++)
            {
                heitonPisteet[i] = 0;
                vuoronPisteet[i] = 0;
                yhteisPisteet[i] = 0;
                kaato[i] = 0;
                paikko[i] = 0;
                heitonNro = 0;
            }
        }
    }
}
