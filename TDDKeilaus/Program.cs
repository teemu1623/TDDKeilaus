﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TDDKeilaus
{
    class Program
    {
        static void Main(string[] args)
        {
            Keilaaja keilaaja1 = new Keilaaja();
            System.Threading.Thread.Sleep(50);
            Keilaaja keilaaja2 = new Keilaaja();
            System.Threading.Thread.Sleep(50);
            Keilaaja keilaaja3 = new Keilaaja();
            System.Threading.Thread.Sleep(50);
            Keilaaja keilaaja4 = new Keilaaja();
            System.Threading.Thread.Sleep(50);
            Keilaaja keilaaja5 = new Keilaaja();
            int x1 = 1;

            Console.WriteLine("Keilaus pistelaskin.\n\nKuinka monta keilaajaa? (1-5):");
            int y = Convert.ToInt32(Console.ReadLine());
            
            if (y >= 1)
            {
                Console.WriteLine("Anna 1. keilaajan nimi: ");
                keilaaja1.Nimi = Console.ReadLine();
            }
            if (y >= 2)
            {
                Console.WriteLine("Anna 2. keilaajan nimi: ");
                keilaaja2.Nimi = Console.ReadLine();
            }
            if (y >= 3)
            {
                Console.WriteLine("Anna 3. keilaajan nimi: ");
                keilaaja3.Nimi = Console.ReadLine();
            }
            if (y >= 4)
            {
                Console.WriteLine("Anna 4. keilaajan nimi: ");
                keilaaja4.Nimi = Console.ReadLine();
            }
            if (y >= 5)
            {
                Console.WriteLine("Anna 5. keilaajan nimi: ");
                keilaaja5.Nimi = Console.ReadLine();
            }
            while (x1 == 1)
            {
                if (y == 1)
                {
                    for (int i = 1; i < 18; i = i + 2)
                    {
                        System.Threading.Thread.Sleep(1000);
                        keilaaja1.heitäYksiVuoro(i);
                        Console.Clear();
                        keilaaja1.TulostaPisteet();
                    }
                    keilaaja1.heitäViimeinenVuoro();
                    System.Threading.Thread.Sleep(1000);
                    Console.Clear();
                    keilaaja1.TulostaPisteet();
                }

                if (y == 2)
                {
                    for (int i = 1; i < 18; i = i + 2)
                    {
                        System.Threading.Thread.Sleep(1000);
                        keilaaja1.heitäYksiVuoro(i);
                        Console.Clear();
                        keilaaja1.TulostaPisteet();
                        keilaaja2.TulostaPisteet();

                        System.Threading.Thread.Sleep(1000);
                        keilaaja2.heitäYksiVuoro(i);
                        Console.Clear();
                        keilaaja1.TulostaPisteet();
                        keilaaja2.TulostaPisteet();
                    }
                    keilaaja1.heitäViimeinenVuoro();
                    System.Threading.Thread.Sleep(1000);
                    Console.Clear();
                    keilaaja1.TulostaPisteet();
                    keilaaja2.TulostaPisteet();

                    keilaaja2.heitäViimeinenVuoro();
                    System.Threading.Thread.Sleep(1000);
                    Console.Clear();
                    keilaaja1.TulostaPisteet();
                    keilaaja2.TulostaPisteet();
                }

                if (y == 3)
                {
                    for (int i = 1; i < 18; i = i + 2)
                    {
                        System.Threading.Thread.Sleep(1000);
                        keilaaja1.heitäYksiVuoro(i);
                        Console.Clear();
                        keilaaja1.TulostaPisteet();
                        keilaaja2.TulostaPisteet();
                        keilaaja3.TulostaPisteet();

                        System.Threading.Thread.Sleep(1000);
                        keilaaja2.heitäYksiVuoro(i);
                        Console.Clear();
                        keilaaja1.TulostaPisteet();
                        keilaaja2.TulostaPisteet();
                        keilaaja3.TulostaPisteet();

                        System.Threading.Thread.Sleep(1000);
                        keilaaja3.heitäYksiVuoro(i);
                        Console.Clear();
                        keilaaja1.TulostaPisteet();
                        keilaaja2.TulostaPisteet();
                        keilaaja3.TulostaPisteet();
                    }
                    keilaaja1.heitäViimeinenVuoro();
                    System.Threading.Thread.Sleep(1000);
                    Console.Clear();
                    keilaaja1.TulostaPisteet();
                    keilaaja2.TulostaPisteet();
                    keilaaja3.TulostaPisteet();

                    keilaaja2.heitäViimeinenVuoro();
                    System.Threading.Thread.Sleep(1000);
                    Console.Clear();
                    keilaaja1.TulostaPisteet();
                    keilaaja2.TulostaPisteet();
                    keilaaja3.TulostaPisteet();

                    keilaaja3.heitäViimeinenVuoro();
                    System.Threading.Thread.Sleep(1000);
                    Console.Clear();
                    keilaaja1.TulostaPisteet();
                    keilaaja2.TulostaPisteet();
                    keilaaja3.TulostaPisteet();
                }

                if (y == 4)
                {
                    for (int i = 1; i < 18; i = i + 2)
                    {
                        System.Threading.Thread.Sleep(1000);
                        keilaaja1.heitäYksiVuoro(i);
                        Console.Clear();
                        keilaaja1.TulostaPisteet();
                        keilaaja2.TulostaPisteet();
                        keilaaja3.TulostaPisteet();
                        keilaaja4.TulostaPisteet();

                        System.Threading.Thread.Sleep(1000);
                        keilaaja2.heitäYksiVuoro(i);
                        Console.Clear();
                        keilaaja1.TulostaPisteet();
                        keilaaja2.TulostaPisteet();
                        keilaaja3.TulostaPisteet();
                        keilaaja4.TulostaPisteet();

                        System.Threading.Thread.Sleep(1000);
                        keilaaja3.heitäYksiVuoro(i);
                        Console.Clear();
                        keilaaja1.TulostaPisteet();
                        keilaaja2.TulostaPisteet();
                        keilaaja3.TulostaPisteet();
                        keilaaja4.TulostaPisteet();

                        System.Threading.Thread.Sleep(1000);
                        keilaaja4.heitäYksiVuoro(i);
                        Console.Clear();
                        keilaaja1.TulostaPisteet();
                        keilaaja2.TulostaPisteet();
                        keilaaja3.TulostaPisteet();
                        keilaaja4.TulostaPisteet();
                    }
                    keilaaja1.heitäViimeinenVuoro();
                    System.Threading.Thread.Sleep(1000);
                    Console.Clear();
                    keilaaja1.TulostaPisteet();
                    keilaaja2.TulostaPisteet();
                    keilaaja3.TulostaPisteet();
                    keilaaja4.TulostaPisteet();

                    keilaaja2.heitäViimeinenVuoro();
                    System.Threading.Thread.Sleep(1000);
                    Console.Clear();
                    keilaaja1.TulostaPisteet();
                    keilaaja2.TulostaPisteet();
                    keilaaja3.TulostaPisteet();
                    keilaaja4.TulostaPisteet();

                    keilaaja3.heitäViimeinenVuoro();
                    System.Threading.Thread.Sleep(1000);
                    Console.Clear();
                    keilaaja1.TulostaPisteet();
                    keilaaja2.TulostaPisteet();
                    keilaaja3.TulostaPisteet();
                    keilaaja4.TulostaPisteet();

                    keilaaja4.heitäViimeinenVuoro();
                    System.Threading.Thread.Sleep(1000);
                    Console.Clear();
                    keilaaja1.TulostaPisteet();
                    keilaaja2.TulostaPisteet();
                    keilaaja3.TulostaPisteet();
                    keilaaja4.TulostaPisteet();
                }

                if (y == 5)
                {
                    for (int i = 1; i < 18; i = i + 2)
                    {
                        System.Threading.Thread.Sleep(1000);
                        keilaaja1.heitäYksiVuoro(i);
                        Console.Clear();
                        keilaaja1.TulostaPisteet();
                        keilaaja2.TulostaPisteet();
                        keilaaja3.TulostaPisteet();
                        keilaaja4.TulostaPisteet();
                        keilaaja5.TulostaPisteet();

                        System.Threading.Thread.Sleep(1000);
                        keilaaja2.heitäYksiVuoro(i);
                        Console.Clear();
                        keilaaja1.TulostaPisteet();
                        keilaaja2.TulostaPisteet();
                        keilaaja3.TulostaPisteet();
                        keilaaja4.TulostaPisteet();
                        keilaaja5.TulostaPisteet();

                        System.Threading.Thread.Sleep(1000);
                        keilaaja3.heitäYksiVuoro(i);
                        Console.Clear();
                        keilaaja1.TulostaPisteet();
                        keilaaja2.TulostaPisteet();
                        keilaaja3.TulostaPisteet();
                        keilaaja4.TulostaPisteet();
                        keilaaja5.TulostaPisteet();

                        System.Threading.Thread.Sleep(1000);
                        keilaaja4.heitäYksiVuoro(i);
                        Console.Clear();
                        keilaaja1.TulostaPisteet();
                        keilaaja2.TulostaPisteet();
                        keilaaja3.TulostaPisteet();
                        keilaaja4.TulostaPisteet();
                        keilaaja5.TulostaPisteet();

                        System.Threading.Thread.Sleep(1000);
                        keilaaja5.heitäYksiVuoro(i);
                        Console.Clear();
                        keilaaja1.TulostaPisteet();
                        keilaaja2.TulostaPisteet();
                        keilaaja3.TulostaPisteet();
                        keilaaja4.TulostaPisteet();
                        keilaaja5.TulostaPisteet();
                    }
                    keilaaja1.heitäViimeinenVuoro();
                    System.Threading.Thread.Sleep(1000);
                    Console.Clear();
                    keilaaja1.TulostaPisteet();
                    keilaaja2.TulostaPisteet();
                    keilaaja3.TulostaPisteet();
                    keilaaja4.TulostaPisteet();
                    keilaaja5.TulostaPisteet();

                    keilaaja2.heitäViimeinenVuoro();
                    System.Threading.Thread.Sleep(1000);
                    Console.Clear();
                    keilaaja1.TulostaPisteet();
                    keilaaja2.TulostaPisteet();
                    keilaaja3.TulostaPisteet();
                    keilaaja4.TulostaPisteet();
                    keilaaja5.TulostaPisteet();

                    keilaaja3.heitäViimeinenVuoro();
                    System.Threading.Thread.Sleep(1000);
                    Console.Clear();
                    keilaaja1.TulostaPisteet();
                    keilaaja2.TulostaPisteet();
                    keilaaja3.TulostaPisteet();
                    keilaaja4.TulostaPisteet();
                    keilaaja5.TulostaPisteet();

                    keilaaja4.heitäViimeinenVuoro();
                    System.Threading.Thread.Sleep(1000);
                    Console.Clear();
                    keilaaja1.TulostaPisteet();
                    keilaaja2.TulostaPisteet();
                    keilaaja3.TulostaPisteet();
                    keilaaja4.TulostaPisteet();
                    keilaaja5.TulostaPisteet();

                    keilaaja5.heitäViimeinenVuoro();
                    System.Threading.Thread.Sleep(1000);
                    Console.Clear();
                    keilaaja1.TulostaPisteet();
                    keilaaja2.TulostaPisteet();
                    keilaaja3.TulostaPisteet();
                    keilaaja4.TulostaPisteet();
                    keilaaja5.TulostaPisteet();
                }
                Console.WriteLine("\nUusi peli? 1-Kyllä 0-Ei");
                int x2 = 1;
                while (x2 == 1)
                {
                    x1 = Convert.ToInt32(Console.ReadLine());
                    if (x1 == 1)
                        x2 = 0;
                    if (x1 == 0)
                    {
                        x2 = 0;
                        Console.WriteLine("\nNäkemiin");
                    }
                    if (x2 == 1)
                        Console.WriteLine("Anna luku 1 tai 0.");

                    keilaaja1.nollaus();
                    keilaaja2.nollaus();
                    keilaaja3.nollaus();
                    keilaaja4.nollaus();
                    keilaaja5.nollaus();
                }
            }
        }
    }
}
