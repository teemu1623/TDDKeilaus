﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using TDDKeilaus;

namespace KeilausTest
{
    [TestFixture]
    public class KeilaajaTest
    {
        Keilaaja keilaaja;
        [SetUp]
        public void TestienAlustus()
        {
            keilaaja = new Keilaaja();
        }

        [Test]
        public void NimenJaPisteidenMuokkaus()
        {
            keilaaja.Nimi = "Pauli";
            keilaaja.Pisteet = 1;

            Assert.That(keilaaja.Nimi, Is.EqualTo("Pauli"));
            Assert.That(keilaaja.Pisteet, Is.EqualTo(1));
        }

        [Test]
        public void KeilojaKaatuuMaxKymmenenMinNolla()
        {
            Assert.That(keilaaja.heitäKeilapalloa(10), Is.LessThanOrEqualTo(10));
            Assert.That(keilaaja.heitäKeilapalloa(10), Is.GreaterThanOrEqualTo(0));
        }

        [Test]
        public void HeittoVuorossaKeilojaKaatuuMaxKymmenenMinNolla()
        {
            keilaaja.heitäYksiVuoro(1);
            Assert.That(keilaaja.heitonPisteet[0] + keilaaja.heitonPisteet[1], Is.LessThanOrEqualTo(10));
            Assert.That(keilaaja.heitonPisteet[0] + keilaaja.heitonPisteet[1], Is.GreaterThanOrEqualTo(0));
        }
        [Test]
        public void EkanHeitonKeilatPoisToisesta()
        {
            keilaaja.heitäYksiVuoro(1);
            Assert.That(10 - keilaaja.heitonPisteet[0], Is.GreaterThanOrEqualTo(keilaaja.heitonPisteet[1]));
        }
        [Test]
        public void KymmenänKaadettuaKeilaaOnKaato()
        {
            keilaaja.heitäYksiVuoro(1);

            if (keilaaja.heitonPisteet[0] == 10)           
                Assert.That(keilaaja.kaato[0], Is.EqualTo(1));           
            else
                Assert.That(keilaaja.kaato[0], Is.EqualTo(0));
        }
        [Test]
        public void ToisellaHeitollaKymmenänKaadettuaKeilaaOnPaikko()
        {
            keilaaja.heitäYksiVuoro(1);

            if (keilaaja.heitonPisteet[0] != 10 && keilaaja.heitonPisteet[0] + keilaaja.heitonPisteet[1] == 10)
                Assert.That(keilaaja.paikko[1], Is.EqualTo(1));
            else
                Assert.That(keilaaja.paikko[1], Is.EqualTo(0));
        }
        [Test]
        public void PisteidenYhteenLasku()
        {
            for (int i = 1; i < 20; i = i + 2)
            {
                keilaaja.heitäYksiVuoro(i);
            }
            keilaaja.heitäViimeinenVuoro();

            Assert.That(keilaaja.yhteisPisteet[1], Is.EqualTo(keilaaja.vuoronPisteet[0] + keilaaja.vuoronPisteet[1]));
        }
    }
}